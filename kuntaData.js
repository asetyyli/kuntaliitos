var kuntaData = {
    "Äänekoski": {
        "tyopaikat": 7852,
        "tekijat": 7688,
        "omavaraisuus": 1.0213319458896983
    },
    "Ähtäri": {
        "tyopaikat": 2402,
        "tekijat": 2446,
        "omavaraisuus": 0.982011447260834
    },
    "Akaa": {
        "tyopaikat": 4748,
        "tekijat": 7063,
        "omavaraisuus": 0.672235593940252
    },
    "Alajärvi": {
        "tyopaikat": 3569,
        "tekijat": 3851,
        "omavaraisuus": 0.926772266943651
    },
    "Alavieska": {
        "tyopaikat": 776,
        "tekijat": 1040,
        "omavaraisuus": 0.7461538461538462
    },
    "Alavus": {
        "tyopaikat": 4654,
        "tekijat": 4832,
        "omavaraisuus": 0.9631622516556292
    },
    "Asikkala": {
        "tyopaikat": 2363,
        "tekijat": 3354,
        "omavaraisuus": 0.7045319022063208
    },
    "Askola": {
        "tyopaikat": 1353,
        "tekijat": 2302,
        "omavaraisuus": 0.5877497827975673
    },
    "Aura": {
        "tyopaikat": 1224,
        "tekijat": 1832,
        "omavaraisuus": 0.6681222707423581
    },
    "Brändö": {
        "tyopaikat": 154,
        "tekijat": 225,
        "omavaraisuus": 0.6844444444444444
    },
    "Eckerö": {
        "tyopaikat": 240,
        "tekijat": 432,
        "omavaraisuus": 0.5555555555555556
    },
    "Enonkoski": {
        "tyopaikat": 427,
        "tekijat": 569,
        "omavaraisuus": 0.7504393673110721
    },
    "Enontekiö": {
        "tyopaikat": 643,
        "tekijat": 718,
        "omavaraisuus": 0.8955431754874652
    },
    "Espoo": {
        "tyopaikat": 115624,
        "tekijat": 123033,
        "omavaraisuus": 0.9397803841245844
    },
    "Eura": {
        "tyopaikat": 5390,
        "tekijat": 5186,
        "omavaraisuus": 1.0393366756652527
    },
    "Eurajoki": {
        "tyopaikat": 2579,
        "tekijat": 2539,
        "omavaraisuus": 1.0157542339503742
    },
    "Evijärvi": {
        "tyopaikat": 919,
        "tekijat": 1116,
        "omavaraisuus": 0.8234767025089605
    },
    "Finström": {
        "tyopaikat": 1020,
        "tekijat": 1235,
        "omavaraisuus": 0.8259109311740891
    },
    "Föglö": {
        "tyopaikat": 184,
        "tekijat": 270,
        "omavaraisuus": 0.6814814814814815
    },
    "Forssa": {
        "tyopaikat": 8724,
        "tekijat": 6984,
        "omavaraisuus": 1.2491408934707904
    },
    "Geta": {
        "tyopaikat": 111,
        "tekijat": 218,
        "omavaraisuus": 0.5091743119266054
    },
    "Haapajärvi": {
        "tyopaikat": 3060,
        "tekijat": 2841,
        "omavaraisuus": 1.0770855332629357
    },
    "Haapavesi": {
        "tyopaikat": 2747,
        "tekijat": 2860,
        "omavaraisuus": 0.9604895104895105
    },
    "Hailuoto": {
        "tyopaikat": 236,
        "tekijat": 342,
        "omavaraisuus": 0.6900584795321637
    },
    "Halsua": {
        "tyopaikat": 454,
        "tekijat": 504,
        "omavaraisuus": 0.9007936507936508
    },
    "Hämeenkoski": {
        "tyopaikat": 523,
        "tekijat": 888,
        "omavaraisuus": 0.588963963963964
    },
    "Hämeenkyrö-Tavastkyro": {
        "tyopaikat": 3220,
        "tekijat": 4364,
        "omavaraisuus": 0.7378551787351054
    },
    "Hämeenlinna": {
        "tyopaikat": 29877,
        "tekijat": 28613,
        "omavaraisuus": 1.0441757243211127
    },
    "Hamina": {
        "tyopaikat": 7158,
        "tekijat": 8308,
        "omavaraisuus": 0.8615792007703419
    },
    "Hammarland": {
        "tyopaikat": 302,
        "tekijat": 735,
        "omavaraisuus": 0.4108843537414966
    },
    "Hankasalmi": {
        "tyopaikat": 1560,
        "tekijat": 1972,
        "omavaraisuus": 0.7910750507099391
    },
    "Hanko": {
        "tyopaikat": 4184,
        "tekijat": 3881,
        "omavaraisuus": 1.0780726616851326
    },
    "Harjavalta": {
        "tyopaikat": 3873,
        "tekijat": 2950,
        "omavaraisuus": 1.3128813559322035
    },
    "Hartola": {
        "tyopaikat": 1246,
        "tekijat": 1234,
        "omavaraisuus": 1.0097244732576987
    },
    "Hattula": {
        "tyopaikat": 2652,
        "tekijat": 4372,
        "omavaraisuus": 0.6065873741994511
    },
    "Hausjärvi": {
        "tyopaikat": 2353,
        "tekijat": 4008,
        "omavaraisuus": 0.5870758483033932
    },
    "Heinävesi": {
        "tyopaikat": 1277,
        "tekijat": 1319,
        "omavaraisuus": 0.9681576952236542
    },
    "Heinola": {
        "tyopaikat": 7474,
        "tekijat": 7600,
        "omavaraisuus": 0.983421052631579
    },
    "Helsinki": {
        "tyopaikat": 380579,
        "tekijat": 290835,
        "omavaraisuus": 1.3085735898361615
    },
    "Hirvensalmi": {
        "tyopaikat": 683,
        "tekijat": 846,
        "omavaraisuus": 0.8073286052009456
    },
    "Hollola": {
        "tyopaikat": 6263,
        "tekijat": 9472,
        "omavaraisuus": 0.6612119932432432
    },
    "Honkajoki": {
        "tyopaikat": 876,
        "tekijat": 795,
        "omavaraisuus": 1.1018867924528302
    },
    "Huittinen": {
        "tyopaikat": 4351,
        "tekijat": 4433,
        "omavaraisuus": 0.9815023685991427
    },
    "Humppila": {
        "tyopaikat": 737,
        "tekijat": 1052,
        "omavaraisuus": 0.7005703422053232
    },
    "Hyrynsalmi": {
        "tyopaikat": 751,
        "tekijat": 858,
        "omavaraisuus": 0.8752913752913752
    },
    "Hyvinkää": {
        "tyopaikat": 18295,
        "tekijat": 20833,
        "omavaraisuus": 0.8781740507848126
    },
    "Ii": {
        "tyopaikat": 2422,
        "tekijat": 3358,
        "omavaraisuus": 0.7212626563430613
    },
    "Iisalmi": {
        "tyopaikat": 8992,
        "tekijat": 8964,
        "omavaraisuus": 1.003123605533244
    },
    "Iitti": {
        "tyopaikat": 2171,
        "tekijat": 2817,
        "omavaraisuus": 0.7706780262690806
    },
    "Ikaalinen": {
        "tyopaikat": 2848,
        "tekijat": 2947,
        "omavaraisuus": 0.9664065151001018
    },
    "Ilmajoki": {
        "tyopaikat": 3308,
        "tekijat": 5105,
        "omavaraisuus": 0.6479921645445641
    },
    "Ilomantsi": {
        "tyopaikat": 1807,
        "tekijat": 1962,
        "omavaraisuus": 0.9209989806320081
    },
    "Imatra": {
        "tyopaikat": 11048,
        "tekijat": 10610,
        "omavaraisuus": 1.041281809613572
    },
    "Inari": {
        "tyopaikat": 2715,
        "tekijat": 2799,
        "omavaraisuus": 0.969989281886388
    },
    "Inkoo": {
        "tyopaikat": 1312,
        "tekijat": 2591,
        "omavaraisuus": 0.5063681976071015
    },
    "Isojoki": {
        "tyopaikat": 810,
        "tekijat": 901,
        "omavaraisuus": 0.8990011098779135
    },
    "Isokyrö": {
        "tyopaikat": 1357,
        "tekijat": 2076,
        "omavaraisuus": 0.6536608863198459
    },
    "Jalasjärvi": {
        "tyopaikat": 3089,
        "tekijat": 3306,
        "omavaraisuus": 0.9343617664851784
    },
    "Jämijärvi": {
        "tyopaikat": 639,
        "tekijat": 796,
        "omavaraisuus": 0.8027638190954773
    },
    "Jämsä": {
        "tyopaikat": 8721,
        "tekijat": 8619,
        "omavaraisuus": 1.0118343195266273
    },
    "Janakkala": {
        "tyopaikat": 5237,
        "tekijat": 7315,
        "omavaraisuus": 0.7159261790840739
    },
    "Järvenpää": {
        "tyopaikat": 11827,
        "tekijat": 19233,
        "omavaraisuus": 0.614932667810534
    },
    "Joensuu": {
        "tyopaikat": 31872,
        "tekijat": 29327,
        "omavaraisuus": 1.0867801002489175
    },
    "Jokioinen": {
        "tyopaikat": 1971,
        "tekijat": 2448,
        "omavaraisuus": 0.8051470588235294
    },
    "Jomala": {
        "tyopaikat": 1855,
        "tekijat": 2106,
        "omavaraisuus": 0.8808167141500475
    },
    "Joroinen": {
        "tyopaikat": 1504,
        "tekijat": 2027,
        "omavaraisuus": 0.7419832264430193
    },
    "Joutsa": {
        "tyopaikat": 1606,
        "tekijat": 1767,
        "omavaraisuus": 0.9088851160158461
    },
    "Juankoski": {
        "tyopaikat": 1713,
        "tekijat": 1884,
        "omavaraisuus": 0.9092356687898089
    },
    "Juuka": {
        "tyopaikat": 1925,
        "tekijat": 1854,
        "omavaraisuus": 1.0382955771305287
    },
    "Juupajoki": {
        "tyopaikat": 740,
        "tekijat": 830,
        "omavaraisuus": 0.891566265060241
    },
    "Juva": {
        "tyopaikat": 2338,
        "tekijat": 2740,
        "omavaraisuus": 0.8532846715328467
    },
    "Jyväskylä": {
        "tyopaikat": 59388,
        "tekijat": 56609,
        "omavaraisuus": 1.0490911339186348
    },
    "Kaarina": {
        "tyopaikat": 9271,
        "tekijat": 14178,
        "omavaraisuus": 0.6539004090844971
    },
    "Kaavi": {
        "tyopaikat": 986,
        "tekijat": 1071,
        "omavaraisuus": 0.9206349206349206
    },
    "Kajaani": {
        "tyopaikat": 15602,
        "tekijat": 15175,
        "omavaraisuus": 1.0281383855024713
    },
    "Kalajoki": {
        "tyopaikat": 4511,
        "tekijat": 5081,
        "omavaraisuus": 0.8878173587876402
    },
    "Kangasala": {
        "tyopaikat": 8449,
        "tekijat": 13244,
        "omavaraisuus": 0.6379492600422833
    },
    "Kangasniemi": {
        "tyopaikat": 1826,
        "tekijat": 2104,
        "omavaraisuus": 0.8678707224334601
    },
    "Kankaanpää": {
        "tyopaikat": 5411,
        "tekijat": 4996,
        "omavaraisuus": 1.08306645316253
    },
    "Kannonkoski": {
        "tyopaikat": 445,
        "tekijat": 517,
        "omavaraisuus": 0.8607350096711799
    },
    "Kannus": {
        "tyopaikat": 2343,
        "tekijat": 2370,
        "omavaraisuus": 0.9886075949367089
    },
    "Karijoki": {
        "tyopaikat": 518,
        "tekijat": 603,
        "omavaraisuus": 0.8590381426202321
    },
    "Karkkila": {
        "tyopaikat": 3385,
        "tekijat": 4031,
        "omavaraisuus": 0.8397419995038452
    },
    "Kärkölä": {
        "tyopaikat": 1777,
        "tekijat": 2096,
        "omavaraisuus": 0.8478053435114504
    },
    "Kärsämäki": {
        "tyopaikat": 892,
        "tekijat": 1005,
        "omavaraisuus": 0.8875621890547264
    },
    "Karstula": {
        "tyopaikat": 1392,
        "tekijat": 1617,
        "omavaraisuus": 0.8608534322820037
    },
    "Karvia": {
        "tyopaikat": 923,
        "tekijat": 999,
        "omavaraisuus": 0.923923923923924
    },
    "Kaskinen": {
        "tyopaikat": 718,
        "tekijat": 551,
        "omavaraisuus": 1.3030852994555353
    },
    "Kauhajoki": {
        "tyopaikat": 5736,
        "tekijat": 5738,
        "omavaraisuus": 0.9996514464970373
    },
    "Kauhava": {
        "tyopaikat": 7252,
        "tekijat": 7060,
        "omavaraisuus": 1.0271954674220962
    },
    "Kauniainen": {
        "tyopaikat": 2257,
        "tekijat": 3686,
        "omavaraisuus": 0.612316874660879
    },
    "Kaustinen": {
        "tyopaikat": 1747,
        "tekijat": 1983,
        "omavaraisuus": 0.880988401412002
    },
    "Keitele": {
        "tyopaikat": 1033,
        "tekijat": 933,
        "omavaraisuus": 1.1071811361200428
    },
    "Kemi": {
        "tyopaikat": 9319,
        "tekijat": 8040,
        "omavaraisuus": 1.1590796019900498
    },
    "Kemijärvi": {
        "tyopaikat": 2607,
        "tekijat": 2761,
        "omavaraisuus": 0.9442231075697212
    },
    "Keminmaa": {
        "tyopaikat": 2316,
        "tekijat": 3497,
        "omavaraisuus": 0.6622819559622534
    },
    "Kemiönsaari": {
        "tyopaikat": 2572,
        "tekijat": 2926,
        "omavaraisuus": 0.8790157211209843
    },
    "Kempele": {
        "tyopaikat": 4674,
        "tekijat": 6824,
        "omavaraisuus": 0.6849355216881594
    },
    "Kerava": {
        "tyopaikat": 11170,
        "tekijat": 16882,
        "omavaraisuus": 0.6616514630967895
    },
    "Keuruu": {
        "tyopaikat": 3832,
        "tekijat": 3956,
        "omavaraisuus": 0.9686552072800809
    },
    "Kihniö": {
        "tyopaikat": 708,
        "tekijat": 847,
        "omavaraisuus": 0.8358913813459268
    },
    "Kinnula": {
        "tyopaikat": 571,
        "tekijat": 654,
        "omavaraisuus": 0.8730886850152905
    },
    "Kirkkonummi": {
        "tyopaikat": 10748,
        "tekijat": 17862,
        "omavaraisuus": 0.6017243309819729
    },
    "Kitee": {
        "tyopaikat": 4418,
        "tekijat": 4209,
        "omavaraisuus": 1.049655500118793
    },
    "Kittilä": {
        "tyopaikat": 2901,
        "tekijat": 2815,
        "omavaraisuus": 1.030550621669627
    },
    "Kiuruvesi": {
        "tyopaikat": 2915,
        "tekijat": 3198,
        "omavaraisuus": 0.9115071919949969
    },
    "Kivijärvi": {
        "tyopaikat": 273,
        "tekijat": 392,
        "omavaraisuus": 0.6964285714285714
    },
    "Kökar": {
        "tyopaikat": 69,
        "tekijat": 109,
        "omavaraisuus": 0.6330275229357798
    },
    "Kokemäki": {
        "tyopaikat": 2684,
        "tekijat": 3142,
        "omavaraisuus": 0.8542329726288987
    },
    "Kokkola": {
        "tyopaikat": 19793,
        "tekijat": 19467,
        "omavaraisuus": 1.0167462885909488
    },
    "Koko maa": {
        "tyopaikat": 2289975,
        "tekijat": 2325679,
        "omavaraisuus": 0.984647924326616
    },
    "Kolari": {
        "tyopaikat": 1461,
        "tekijat": 1590,
        "omavaraisuus": 0.9188679245283019
    },
    "Konnevesi": {
        "tyopaikat": 812,
        "tekijat": 1098,
        "omavaraisuus": 0.7395264116575592
    },
    "Kontiolahti": {
        "tyopaikat": 3622,
        "tekijat": 5967,
        "omavaraisuus": 0.6070051952404893
    },
    "Korsnäs": {
        "tyopaikat": 800,
        "tekijat": 1008,
        "omavaraisuus": 0.7936507936507936
    },
    "Koski Tl": {
        "tyopaikat": 925,
        "tekijat": 976,
        "omavaraisuus": 0.9477459016393442
    },
    "Kotka": {
        "tyopaikat": 22763,
        "tekijat": 21156,
        "omavaraisuus": 1.0759595386651541
    },
    "Kouvola": {
        "tyopaikat": 34652,
        "tekijat": 35489,
        "omavaraisuus": 0.9764152272535151
    },
    "Köyliö": {
        "tyopaikat": 690,
        "tekijat": 1204,
        "omavaraisuus": 0.5730897009966778
    },
    "Kristiinankaupunki": {
        "tyopaikat": 2642,
        "tekijat": 2891,
        "omavaraisuus": 0.9138706329989623
    },
    "Kruunupyy": {
        "tyopaikat": 2555,
        "tekijat": 2963,
        "omavaraisuus": 0.8623017212284847
    },
    "Kuhmo": {
        "tyopaikat": 3057,
        "tekijat": 3217,
        "omavaraisuus": 0.9502642213242151
    },
    "Kuhmoinen": {
        "tyopaikat": 654,
        "tekijat": 836,
        "omavaraisuus": 0.7822966507177034
    },
    "Kumlinge": {
        "tyopaikat": 79,
        "tekijat": 156,
        "omavaraisuus": 0.5064102564102564
    },
    "Kuopio": {
        "tyopaikat": 47097,
        "tekijat": 43850,
        "omavaraisuus": 1.0740478905359179
    },
    "Kuortane": {
        "tyopaikat": 1391,
        "tekijat": 1537,
        "omavaraisuus": 0.9050097592713078
    },
    "Kurikka": {
        "tyopaikat": 4617,
        "tekijat": 5881,
        "omavaraisuus": 0.785070566230233
    },
    "Kustavi": {
        "tyopaikat": 307,
        "tekijat": 355,
        "omavaraisuus": 0.8647887323943662
    },
    "Kuusamo": {
        "tyopaikat": 6305,
        "tekijat": 6319,
        "omavaraisuus": 0.9977844595663871
    },
    "Kyyjärvi": {
        "tyopaikat": 541,
        "tekijat": 562,
        "omavaraisuus": 0.9626334519572953
    },
    "Lahti": {
        "tyopaikat": 45693,
        "tekijat": 41767,
        "omavaraisuus": 1.0939976536500107
    },
    "Laihia": {
        "tyopaikat": 1881,
        "tekijat": 3530,
        "omavaraisuus": 0.5328611898016997
    },
    "Laitila": {
        "tyopaikat": 3773,
        "tekijat": 3646,
        "omavaraisuus": 1.034832693362589
    },
    "Lapinjärvi": {
        "tyopaikat": 1019,
        "tekijat": 1200,
        "omavaraisuus": 0.8491666666666666
    },
    "Lapinlahti": {
        "tyopaikat": 3222,
        "tekijat": 3849,
        "omavaraisuus": 0.8371005455962588
    },
    "Lappajärvi": {
        "tyopaikat": 1183,
        "tekijat": 1305,
        "omavaraisuus": 0.9065134099616858
    },
    "Lappeenranta": {
        "tyopaikat": 30911,
        "tekijat": 29726,
        "omavaraisuus": 1.039864092040638
    },
    "Lapua": {
        "tyopaikat": 4679,
        "tekijat": 6036,
        "omavaraisuus": 0.7751822398939695
    },
    "Laukaa": {
        "tyopaikat": 5374,
        "tekijat": 7587,
        "omavaraisuus": 0.70831685778305
    },
    "Lavia": {
        "tyopaikat": 581,
        "tekijat": 727,
        "omavaraisuus": 0.7991746905089409
    },
    "Lemi": {
        "tyopaikat": 584,
        "tekijat": 1291,
        "omavaraisuus": 0.45236250968241676
    },
    "Lemland": {
        "tyopaikat": 274,
        "tekijat": 877,
        "omavaraisuus": 0.3124287343215507
    },
    "Lempäälä": {
        "tyopaikat": 6263,
        "tekijat": 9236,
        "omavaraisuus": 0.6781074058033781
    },
    "Leppävirta": {
        "tyopaikat": 3009,
        "tekijat": 3954,
        "omavaraisuus": 0.7610015174506829
    },
    "Lestijärvi": {
        "tyopaikat": 310,
        "tekijat": 318,
        "omavaraisuus": 0.9748427672955975
    },
    "Lieksa": {
        "tyopaikat": 4060,
        "tekijat": 4187,
        "omavaraisuus": 0.969668020062097
    },
    "Lieto": {
        "tyopaikat": 5131,
        "tekijat": 7450,
        "omavaraisuus": 0.6887248322147651
    },
    "Liminka": {
        "tyopaikat": 1865,
        "tekijat": 3495,
        "omavaraisuus": 0.5336194563662375
    },
    "Liperi": {
        "tyopaikat": 3394,
        "tekijat": 5043,
        "omavaraisuus": 0.6730120959746183
    },
    "Lohja": {
        "tyopaikat": 17940,
        "tekijat": 24217,
        "omavaraisuus": 0.7408019160094149
    },
    "Loimaa": {
        "tyopaikat": 6836,
        "tekijat": 6943,
        "omavaraisuus": 0.9845887944692496
    },
    "Loppi": {
        "tyopaikat": 2159,
        "tekijat": 3585,
        "omavaraisuus": 0.6022315202231521
    },
    "Loviisa": {
        "tyopaikat": 5726,
        "tekijat": 6617,
        "omavaraisuus": 0.8653468339126492
    },
    "Luhanka": {
        "tyopaikat": 202,
        "tekijat": 256,
        "omavaraisuus": 0.7890625
    },
    "Lumijoki": {
        "tyopaikat": 485,
        "tekijat": 737,
        "omavaraisuus": 0.6580732700135685
    },
    "Lumparland": {
        "tyopaikat": 90,
        "tekijat": 180,
        "omavaraisuus": 0.5
    },
    "Luoto": {
        "tyopaikat": 954,
        "tekijat": 1982,
        "omavaraisuus": 0.48133198789101916
    },
    "Luumäki": {
        "tyopaikat": 1601,
        "tekijat": 1988,
        "omavaraisuus": 0.8053319919517102
    },
    "Luvia": {
        "tyopaikat": 728,
        "tekijat": 1383,
        "omavaraisuus": 0.5263919016630514
    },
    "Maalahti": {
        "tyopaikat": 1829,
        "tekijat": 2522,
        "omavaraisuus": 0.725218080888184
    },
    "Maaninka": {
        "tyopaikat": 951,
        "tekijat": 1515,
        "omavaraisuus": 0.6277227722772277
    },
    "Maarianhamina": {
        "tyopaikat": 10154,
        "tekijat": 5522,
        "omavaraisuus": 1.838826512133285
    },
    "Mäntsälä": {
        "tyopaikat": 5519,
        "tekijat": 9272,
        "omavaraisuus": 0.5952329594477999
    },
    "Mänttä-Vilppula": {
        "tyopaikat": 4481,
        "tekijat": 4304,
        "omavaraisuus": 1.041124535315985
    },
    "Mäntyharju": {
        "tyopaikat": 2094,
        "tekijat": 2421,
        "omavaraisuus": 0.8649318463444857
    },
    "Marttila": {
        "tyopaikat": 552,
        "tekijat": 866,
        "omavaraisuus": 0.6374133949191686
    },
    "Masku": {
        "tyopaikat": 2554,
        "tekijat": 4479,
        "omavaraisuus": 0.570216566197812
    },
    "Merijärvi": {
        "tyopaikat": 259,
        "tekijat": 414,
        "omavaraisuus": 0.6256038647342995
    },
    "Merikarvia": {
        "tyopaikat": 980,
        "tekijat": 1171,
        "omavaraisuus": 0.8368915456874466
    },
    "Miehikkälä": {
        "tyopaikat": 583,
        "tekijat": 779,
        "omavaraisuus": 0.748395378690629
    },
    "Mikkeli": {
        "tyopaikat": 23349,
        "tekijat": 22595,
        "omavaraisuus": 1.0333702146492587
    },
    "Muhos": {
        "tyopaikat": 2509,
        "tekijat": 3350,
        "omavaraisuus": 0.748955223880597
    },
    "Multia": {
        "tyopaikat": 564,
        "tekijat": 645,
        "omavaraisuus": 0.8744186046511628
    },
    "Muonio": {
        "tyopaikat": 963,
        "tekijat": 990,
        "omavaraisuus": 0.9727272727272728
    },
    "Mustasaari": {
        "tyopaikat": 4789,
        "tekijat": 9021,
        "omavaraisuus": 0.5308724088238554
    },
    "Muurame": {
        "tyopaikat": 2580,
        "tekijat": 4167,
        "omavaraisuus": 0.619150467962563
    },
    "Mynämäki": {
        "tyopaikat": 2120,
        "tekijat": 3483,
        "omavaraisuus": 0.608670686190066
    },
    "Myrskylä": {
        "tyopaikat": 530,
        "tekijat": 819,
        "omavaraisuus": 0.6471306471306472
    },
    "Naantali": {
        "tyopaikat": 5952,
        "tekijat": 8422,
        "omavaraisuus": 0.7067204939444313
    },
    "Nakkila": {
        "tyopaikat": 1707,
        "tekijat": 2287,
        "omavaraisuus": 0.7463926541320507
    },
    "Närpiö": {
        "tyopaikat": 4343,
        "tekijat": 4277,
        "omavaraisuus": 1.0154313771335048
    },
    "Nastola": {
        "tyopaikat": 5535,
        "tekijat": 6461,
        "omavaraisuus": 0.8566785327348707
    },
    "Nivala": {
        "tyopaikat": 3526,
        "tekijat": 4115,
        "omavaraisuus": 0.856865127582017
    },
    "Nokia": {
        "tyopaikat": 10265,
        "tekijat": 13623,
        "omavaraisuus": 0.7535051016662997
    },
    "Nousiainen": {
        "tyopaikat": 1108,
        "tekijat": 2243,
        "omavaraisuus": 0.49398127507802053
    },
    "Nurmes": {
        "tyopaikat": 2993,
        "tekijat": 2929,
        "omavaraisuus": 1.0218504609081598
    },
    "Nurmijärvi": {
        "tyopaikat": 11129,
        "tekijat": 19160,
        "omavaraisuus": 0.5808455114822547
    },
    "Orimattila": {
        "tyopaikat": 4753,
        "tekijat": 6791,
        "omavaraisuus": 0.6998969223972905
    },
    "Oripää": {
        "tyopaikat": 431,
        "tekijat": 588,
        "omavaraisuus": 0.7329931972789115
    },
    "Orivesi": {
        "tyopaikat": 2856,
        "tekijat": 3719,
        "omavaraisuus": 0.7679483732186072
    },
    "Oulainen": {
        "tyopaikat": 2800,
        "tekijat": 2893,
        "omavaraisuus": 0.9678534393363291
    },
    "Oulu": {
        "tyopaikat": 82139,
        "tekijat": 79028,
        "omavaraisuus": 1.039365794401984
    },
    "Outokumpu": {
        "tyopaikat": 2466,
        "tekijat": 2536,
        "omavaraisuus": 0.972397476340694
    },
    "Padasjoki": {
        "tyopaikat": 922,
        "tekijat": 1198,
        "omavaraisuus": 0.7696160267111853
    },
    "Paimio": {
        "tyopaikat": 3598,
        "tekijat": 4819,
        "omavaraisuus": 0.7466279311060386
    },
    "Pälkäne": {
        "tyopaikat": 2166,
        "tekijat": 2787,
        "omavaraisuus": 0.7771797631862217
    },
    "Paltamo": {
        "tyopaikat": 1176,
        "tekijat": 1461,
        "omavaraisuus": 0.8049281314168378
    },
    "Parainen": {
        "tyopaikat": 5384,
        "tekijat": 6779,
        "omavaraisuus": 0.7942174362000295
    },
    "Parikkala": {
        "tyopaikat": 1780,
        "tekijat": 2017,
        "omavaraisuus": 0.8824987605354487
    },
    "Parkano": {
        "tyopaikat": 2746,
        "tekijat": 2653,
        "omavaraisuus": 1.0350546551074256
    },
    "Pedersören kunta": {
        "tyopaikat": 3307,
        "tekijat": 4856,
        "omavaraisuus": 0.681013179571664
    },
    "Pelkosenniemi": {
        "tyopaikat": 374,
        "tekijat": 350,
        "omavaraisuus": 1.0685714285714285
    },
    "Pello": {
        "tyopaikat": 1261,
        "tekijat": 1330,
        "omavaraisuus": 0.9481203007518797
    },
    "Perho": {
        "tyopaikat": 940,
        "tekijat": 1040,
        "omavaraisuus": 0.9038461538461539
    },
    "Pertunmaa": {
        "tyopaikat": 673,
        "tekijat": 704,
        "omavaraisuus": 0.9559659090909091
    },
    "Petäjävesi": {
        "tyopaikat": 950,
        "tekijat": 1579,
        "omavaraisuus": 0.6016466117796073
    },
    "Pieksämäki": {
        "tyopaikat": 7452,
        "tekijat": 7592,
        "omavaraisuus": 0.9815595363540569
    },
    "Pielavesi": {
        "tyopaikat": 1478,
        "tekijat": 1643,
        "omavaraisuus": 0.8995739500912964
    },
    "Pietarsaari": {
        "tyopaikat": 10230,
        "tekijat": 8047,
        "omavaraisuus": 1.2712812228159562
    },
    "Pihtipudas": {
        "tyopaikat": 1515,
        "tekijat": 1583,
        "omavaraisuus": 0.9570435881238155
    },
    "Pirkkala": {
        "tyopaikat": 4835,
        "tekijat": 8006,
        "omavaraisuus": 0.6039220584561579
    },
    "Polvijärvi": {
        "tyopaikat": 1302,
        "tekijat": 1698,
        "omavaraisuus": 0.7667844522968198
    },
    "Pomarkku": {
        "tyopaikat": 558,
        "tekijat": 869,
        "omavaraisuus": 0.6421173762945915
    },
    "Pori": {
        "tyopaikat": 35534,
        "tekijat": 33453,
        "omavaraisuus": 1.062206678025887
    },
    "Pornainen": {
        "tyopaikat": 915,
        "tekijat": 2388,
        "omavaraisuus": 0.38316582914572866
    },
    "Porvoo": {
        "tyopaikat": 20244,
        "tekijat": 22520,
        "omavaraisuus": 0.8989342806394316
    },
    "Posio": {
        "tyopaikat": 1211,
        "tekijat": 1319,
        "omavaraisuus": 0.9181197877179682
    },
    "Pöytyä": {
        "tyopaikat": 2813,
        "tekijat": 3602,
        "omavaraisuus": 0.7809550249861188
    },
    "Pudasjärvi": {
        "tyopaikat": 2637,
        "tekijat": 2802,
        "omavaraisuus": 0.9411134903640257
    },
    "Pukkila": {
        "tyopaikat": 461,
        "tekijat": 895,
        "omavaraisuus": 0.5150837988826815
    },
    "Punkalaidun": {
        "tyopaikat": 1097,
        "tekijat": 1204,
        "omavaraisuus": 0.9111295681063123
    },
    "Puolanka": {
        "tyopaikat": 901,
        "tekijat": 953,
        "omavaraisuus": 0.9454354669464848
    },
    "Puumala": {
        "tyopaikat": 829,
        "tekijat": 888,
        "omavaraisuus": 0.9335585585585585
    },
    "Pyhäjärvi": {
        "tyopaikat": 1788,
        "tekijat": 1977,
        "omavaraisuus": 0.9044006069802731
    },
    "Pyhäjoki": {
        "tyopaikat": 768,
        "tekijat": 1282,
        "omavaraisuus": 0.5990639625585024
    },
    "Pyhäntä": {
        "tyopaikat": 727,
        "tekijat": 626,
        "omavaraisuus": 1.1613418530351438
    },
    "Pyhäranta": {
        "tyopaikat": 545,
        "tekijat": 967,
        "omavaraisuus": 0.5635987590486039
    },
    "Pyhtää": {
        "tyopaikat": 991,
        "tekijat": 2226,
        "omavaraisuus": 0.44519317160826594
    },
    "Raahe": {
        "tyopaikat": 10339,
        "tekijat": 8882,
        "omavaraisuus": 1.1640396307138032
    },
    "Rääkkylä": {
        "tyopaikat": 702,
        "tekijat": 843,
        "omavaraisuus": 0.8327402135231317
    },
    "Raasepori": {
        "tyopaikat": 10952,
        "tekijat": 12607,
        "omavaraisuus": 0.8687237249147299
    },
    "Raisio": {
        "tyopaikat": 9741,
        "tekijat": 10798,
        "omavaraisuus": 0.9021115021300241
    },
    "Rantasalmi": {
        "tyopaikat": 1194,
        "tekijat": 1432,
        "omavaraisuus": 0.8337988826815642
    },
    "Ranua": {
        "tyopaikat": 1286,
        "tekijat": 1408,
        "omavaraisuus": 0.9133522727272727
    },
    "Rauma": {
        "tyopaikat": 17216,
        "tekijat": 17023,
        "omavaraisuus": 1.0113376020677907
    },
    "Rautalampi": {
        "tyopaikat": 997,
        "tekijat": 1181,
        "omavaraisuus": 0.8441998306519899
    },
    "Rautavaara": {
        "tyopaikat": 540,
        "tekijat": 579,
        "omavaraisuus": 0.9326424870466321
    },
    "Rautjärvi": {
        "tyopaikat": 1163,
        "tekijat": 1350,
        "omavaraisuus": 0.8614814814814815
    },
    "Reisjärvi": {
        "tyopaikat": 1000,
        "tekijat": 1124,
        "omavaraisuus": 0.8896797153024911
    },
    "Riihimäki": {
        "tyopaikat": 11527,
        "tekijat": 12902,
        "omavaraisuus": 0.8934273756006821
    },
    "Ristijärvi": {
        "tyopaikat": 398,
        "tekijat": 527,
        "omavaraisuus": 0.7552182163187856
    },
    "Rovaniemi": {
        "tyopaikat": 24479,
        "tekijat": 25004,
        "omavaraisuus": 0.979003359462486
    },
    "Ruokolahti": {
        "tyopaikat": 1206,
        "tekijat": 2076,
        "omavaraisuus": 0.5809248554913294
    },
    "Ruovesi": {
        "tyopaikat": 2066,
        "tekijat": 1877,
        "omavaraisuus": 1.1006925945657964
    },
    "Rusko": {
        "tyopaikat": 1555,
        "tekijat": 2739,
        "omavaraisuus": 0.5677254472435196
    },
    "Saarijärvi": {
        "tyopaikat": 3699,
        "tekijat": 3805,
        "omavaraisuus": 0.9721419185282523
    },
    "Säkylä": {
        "tyopaikat": 2556,
        "tekijat": 2112,
        "omavaraisuus": 1.2102272727272727
    },
    "Salla": {
        "tyopaikat": 1237,
        "tekijat": 1315,
        "omavaraisuus": 0.9406844106463879
    },
    "Salo": {
        "tyopaikat": 23930,
        "tekijat": 23130,
        "omavaraisuus": 1.0345871162991787
    },
    "Saltvik": {
        "tyopaikat": 419,
        "tekijat": 873,
        "omavaraisuus": 0.47995418098510884
    },
    "Sastamala": {
        "tyopaikat": 8995,
        "tekijat": 10361,
        "omavaraisuus": 0.8681594440691053
    },
    "Sauvo": {
        "tyopaikat": 796,
        "tekijat": 1351,
        "omavaraisuus": 0.5891931902294597
    },
    "Savitaipale": {
        "tyopaikat": 1251,
        "tekijat": 1388,
        "omavaraisuus": 0.9012968299711815
    },
    "Savonlinna": {
        "tyopaikat": 13788,
        "tekijat": 13914,
        "omavaraisuus": 0.9909443725743855
    },
    "Savukoski": {
        "tyopaikat": 391,
        "tekijat": 442,
        "omavaraisuus": 0.8846153846153846
    },
    "Seinäjoki": {
        "tyopaikat": 28548,
        "tekijat": 26070,
        "omavaraisuus": 1.0950517836593785
    },
    "Sievi": {
        "tyopaikat": 2221,
        "tekijat": 1926,
        "omavaraisuus": 1.1531671858774664
    },
    "Siikainen": {
        "tyopaikat": 487,
        "tekijat": 572,
        "omavaraisuus": 0.8513986013986014
    },
    "Siikajoki": {
        "tyopaikat": 1649,
        "tekijat": 2098,
        "omavaraisuus": 0.7859866539561488
    },
    "Siikalatva": {
        "tyopaikat": 2177,
        "tekijat": 2342,
        "omavaraisuus": 0.9295473953885568
    },
    "Siilinjärvi": {
        "tyopaikat": 6445,
        "tekijat": 9283,
        "omavaraisuus": 0.6942798664224927
    },
    "Simo": {
        "tyopaikat": 667,
        "tekijat": 1216,
        "omavaraisuus": 0.5485197368421053
    },
    "Sipoo": {
        "tyopaikat": 5043,
        "tekijat": 8703,
        "omavaraisuus": 0.5794553602206136
    },
    "Sodankylä": {
        "tyopaikat": 3352,
        "tekijat": 3493,
        "omavaraisuus": 0.9596335528199256
    },
    "Soini": {
        "tyopaikat": 958,
        "tekijat": 819,
        "omavaraisuus": 1.1697191697191698
    },
    "Somero": {
        "tyopaikat": 3003,
        "tekijat": 3633,
        "omavaraisuus": 0.8265895953757225
    },
    "Sonkajärvi": {
        "tyopaikat": 1487,
        "tekijat": 1636,
        "omavaraisuus": 0.9089242053789731
    },
    "Sotkamo": {
        "tyopaikat": 3972,
        "tekijat": 4225,
        "omavaraisuus": 0.9401183431952663
    },
    "Sottunga": {
        "tyopaikat": 35,
        "tekijat": 55,
        "omavaraisuus": 0.6363636363636364
    },
    "Sulkava": {
        "tyopaikat": 877,
        "tekijat": 1025,
        "omavaraisuus": 0.855609756097561
    },
    "Sund": {
        "tyopaikat": 203,
        "tekijat": 506,
        "omavaraisuus": 0.40118577075098816
    },
    "Suomussalmi": {
        "tyopaikat": 2844,
        "tekijat": 3138,
        "omavaraisuus": 0.9063097514340345
    },
    "Suonenjoki": {
        "tyopaikat": 2601,
        "tekijat": 2816,
        "omavaraisuus": 0.9236505681818182
    },
    "Sysmä": {
        "tyopaikat": 1319,
        "tekijat": 1488,
        "omavaraisuus": 0.8864247311827957
    },
    "Taipalsaari": {
        "tyopaikat": 1065,
        "tekijat": 2077,
        "omavaraisuus": 0.5127587867116032
    },
    "Taivalkoski": {
        "tyopaikat": 1262,
        "tekijat": 1446,
        "omavaraisuus": 0.8727524204702628
    },
    "Taivassalo": {
        "tyopaikat": 502,
        "tekijat": 695,
        "omavaraisuus": 0.7223021582733813
    },
    "Tammela": {
        "tyopaikat": 1799,
        "tekijat": 2775,
        "omavaraisuus": 0.6482882882882883
    },
    "Tampere": {
        "tyopaikat": 114120,
        "tekijat": 94200,
        "omavaraisuus": 1.2114649681528662
    },
    "Tarvasjoki": {
        "tyopaikat": 500,
        "tekijat": 848,
        "omavaraisuus": 0.589622641509434
    },
    "Tervo": {
        "tyopaikat": 454,
        "tekijat": 608,
        "omavaraisuus": 0.7467105263157895
    },
    "Tervola": {
        "tyopaikat": 1044,
        "tekijat": 1155,
        "omavaraisuus": 0.9038961038961039
    },
    "Teuva": {
        "tyopaikat": 1965,
        "tekijat": 2272,
        "omavaraisuus": 0.8648767605633803
    },
    "Tohmajärvi": {
        "tyopaikat": 1567,
        "tekijat": 1766,
        "omavaraisuus": 0.8873159682899208
    },
    "Toholampi": {
        "tyopaikat": 1236,
        "tekijat": 1382,
        "omavaraisuus": 0.894356005788712
    },
    "Toivakka": {
        "tyopaikat": 621,
        "tekijat": 928,
        "omavaraisuus": 0.6691810344827587
    },
    "Tornio": {
        "tyopaikat": 9061,
        "tekijat": 8949,
        "omavaraisuus": 1.012515364845234
    },
    "Turku": {
        "tyopaikat": 94352,
        "tekijat": 76787,
        "omavaraisuus": 1.228749658145259
    },
    "Tuusniemi": {
        "tyopaikat": 768,
        "tekijat": 980,
        "omavaraisuus": 0.7836734693877551
    },
    "Tuusula": {
        "tyopaikat": 13414,
        "tekijat": 18136,
        "omavaraisuus": 0.7396338773709749
    },
    "Tyrnävä": {
        "tyopaikat": 1367,
        "tekijat": 2398,
        "omavaraisuus": 0.5700583819849875
    },
    "Ulvila": {
        "tyopaikat": 3820,
        "tekijat": 5687,
        "omavaraisuus": 0.6717074028486021
    },
    "Urjala": {
        "tyopaikat": 1744,
        "tekijat": 2016,
        "omavaraisuus": 0.8650793650793651
    },
    "Utajärvi": {
        "tyopaikat": 920,
        "tekijat": 1013,
        "omavaraisuus": 0.9081934846989141
    },
    "Utsjoki": {
        "tyopaikat": 505,
        "tekijat": 502,
        "omavaraisuus": 1.0059760956175299
    },
    "Uurainen": {
        "tyopaikat": 812,
        "tekijat": 1368,
        "omavaraisuus": 0.5935672514619883
    },
    "Uusikaarlepyy": {
        "tyopaikat": 3203,
        "tekijat": 3358,
        "omavaraisuus": 0.9538415723645027
    },
    "Uusikaupunki": {
        "tyopaikat": 6265,
        "tekijat": 6484,
        "omavaraisuus": 0.966224552745219
    },
    "Vaala": {
        "tyopaikat": 961,
        "tekijat": 1098,
        "omavaraisuus": 0.8752276867030966
    },
    "Vaasa": {
        "tyopaikat": 36685,
        "tekijat": 29174,
        "omavaraisuus": 1.257455268389662
    },
    "Valkeakoski": {
        "tyopaikat": 7487,
        "tekijat": 8301,
        "omavaraisuus": 0.9019395253583905
    },
    "Valtimo": {
        "tyopaikat": 733,
        "tekijat": 825,
        "omavaraisuus": 0.8884848484848484
    },
    "Vantaa": {
        "tyopaikat": 101134,
        "tekijat": 99594,
        "omavaraisuus": 1.0154627788822619
    },
    "Vårdö": {
        "tyopaikat": 117,
        "tekijat": 187,
        "omavaraisuus": 0.6256684491978609
    },
    "Varkaus": {
        "tyopaikat": 8978,
        "tekijat": 8215,
        "omavaraisuus": 1.0928788800973828
    },
    "Vehmaa": {
        "tyopaikat": 757,
        "tekijat": 988,
        "omavaraisuus": 0.7661943319838057
    },
    "Vesanto": {
        "tyopaikat": 653,
        "tekijat": 817,
        "omavaraisuus": 0.799265605875153
    },
    "Vesilahti": {
        "tyopaikat": 793,
        "tekijat": 1845,
        "omavaraisuus": 0.42981029810298105
    },
    "Veteli": {
        "tyopaikat": 1206,
        "tekijat": 1411,
        "omavaraisuus": 0.8547129695251595
    },
    "Vieremä": {
        "tyopaikat": 1739,
        "tekijat": 1601,
        "omavaraisuus": 1.0861961274203622
    },
    "Vihanti": {
        "tyopaikat": 861,
        "tekijat": 1076,
        "omavaraisuus": 0.800185873605948
    },
    "Vihti": {
        "tyopaikat": 7954,
        "tekijat": 13523,
        "omavaraisuus": 0.5881830954669822
    },
    "Viitasaari": {
        "tyopaikat": 2438,
        "tekijat": 2586,
        "omavaraisuus": 0.94276875483372
    },
    "Vimpeli": {
        "tyopaikat": 1070,
        "tekijat": 1260,
        "omavaraisuus": 0.8492063492063492
    },
    "Virolahti": {
        "tyopaikat": 1337,
        "tekijat": 1365,
        "omavaraisuus": 0.9794871794871794
    },
    "Virrat": {
        "tyopaikat": 2539,
        "tekijat": 2764,
        "omavaraisuus": 0.9185962373371924
    },
    "Vöyri": {
        "tyopaikat": 2686,
        "tekijat": 3034,
        "omavaraisuus": 0.8852999340804218
    },
    "Ylitornio": {
        "tyopaikat": 1374,
        "tekijat": 1536,
        "omavaraisuus": 0.89453125
    },
    "Ylivieska": {
        "tyopaikat": 5902,
        "tekijat": 5842,
        "omavaraisuus": 1.0102704553235193
    },
    "Ylöjärvi": {
        "tyopaikat": 8808,
        "tekijat": 13496,
        "omavaraisuus": 0.6526378186129224
    },
    "Ypäjä": {
        "tyopaikat": 772,
        "tekijat": 1079,
        "omavaraisuus": 0.7154772937905468
    }
};