var colorScale = new chroma.ColorScale({
	colors: ['#ff0000','#aaaaaa','#00ff00'],
	positions: [0,.5,1],
	mode: 'rgb'
});

var OMAVAR_MAX = 1.2,
	OMAVAR_MIN = 0.8;

// Anna yksittäisen kunnan väri datan perusteella
function alue_vari(alue) {
	if (kuntaData[alue] !== undefined) {
		var omavar = kuntaData[alue].omavaraisuus,
			color_pos = (omavar - OMAVAR_MIN) / (OMAVAR_MAX - OMAVAR_MIN);
		color_pos = Math.max(0, Math.min(color_pos, 1));
		return colorScale.getColor(color_pos).hex();
	} else {
		return "#c5c5c5";
	}
}

// Piirtää Suomen kartan Raphaël-kirjaston avulla
// http://www.raphaeljs.com/
function piirra_kartta() {
	rsr = new ScaleRaphael("paper", 2000, 3500);

	suomi = {};

	$.each(kuntaPaths, function(name, path) {
		suomi[name] = rsr.path(path).attr({
			fill: alue_vari(name),
			stroke: "#fff",
			"stroke-width": $.ipad_or_ie ? 1 : 3
		});
	});

	rsr.scaleAll(.17);		// Kartan aloitusskaala.
	$.suomen_skaala = .17;	// Löytyy jatkossa tästä muuttujasta.
	$.nopeus = .04;			// Miten nopeasti Suomi zoomaa, kun kuntaa klikataan.
	$.vaihe = 1;			// Olemme alussa, siis vaiheessa 1.

	// Annetaan kullekin kunnalle hiirieventit.
	for (var alue in suomi) {

		(function (st, alue) {
			st[0].style.cursor = "pointer";	// Hiiren osoittimen vieminen kunnan
											// päälle näyttää sormikursorin.

			st[0].onmouseup = function(e) {
				if ($.vaihe == 1) {	// Jos ollaan alussa, kunnan klikkaus
									// zoomaa kartan valittuun kuntaan.
					st[0].style.cursor = "default";
					st.toFront();
					rsr.safari();
					$.ss = setInterval("skaalaa_suomi()",20);
					$("#kuntavalinta").val(alue);
					$.vaihe = 2;
					$.valittu = alue;
					var x = suomi[alue].getBBox(true).x2 - (suomi[alue].getBBox(true).width / 5);
					var y = suomi[alue].getBBox(true).y2 - (suomi[alue].getBBox(true).height / 5);
					$("#selite").hide();
				} else if ($.vaihe == 2) {	// Jos oma kunta on jo valittu,
											// merkitään käyttäjän kuntavalinnat.
					if (alue != $.valittu && !(alue in $.valitut)) {
						// Lisätään tämä kunta valittuihin.
						var x = suomi[alue].getBBox(true).x2 - (suomi[alue].getBBox(true).width / 5);
						var y = suomi[alue].getBBox(true).y2 - (suomi[alue].getBBox(true).height / 5);
						$.valitut[alue] = true;
					} else if (alue != $.valittu) {
						// Poistetaan tämä kunta valituista.
						st.attr("fill","#efefef");
						delete $.valitut[alue];
					}
					// Kirjoitetaan valittujen kuntien lista näkyviin.
					listaa_valitut();
				}
			}

			st[0].onmouseover = function(e) {
				if ($.vaihe == 1) {	// Näytetään hiiren alla oleva kunta myös pudotusvalikossa valittuna.
					$("#kuntavalinta").val(alue);
				} else if ($.vaihe == 2) {
					if (alue != $.valittu && !(alue in $.valitut)) {
						st.attr("fill","#efefef");
					}
				}
				if ($.vaihe != 3) {
					if ($.ipad_or_ie) {	// Hitusen eri toiminnallisuus Internet Explorer 8:lle (ja vanhemmille),
										// joka ei suostunut näyttämään kunnan nimeä hiiren kursorin kohdalla.
										// iPad ei tätä noteeraa, kun ei tunnista koko mouseoveria.
						if ($.vaihe == 2) {
							$("#valitut_ie").html("+ " + alue).css("left","250px").css("top","8px").show();
						}
					} else {	// Näytetään kunnan nimi hiiren kursorin kieppeillä.
						var s_x = e.pageX-50;
						var s_y = e.pageY-50;
						if (s_x < 0) s_x = 0;
						if (s_y < 0) s_y = 0;

						// Näytetään selitteessä kunnan omavaraisuus
						var selite = alue;
						if (kuntaData[alue] !== undefined) {
							var omavar = Math.round(kuntaData[alue].omavaraisuus * 100);
							selite += ": " + omavar + "%";
						}
						$("#selite").html(selite).css("left",s_x+"px").css("top",s_y+"px").show();
					}

				}
			}

			// Hiiren osoitin poistuu kunnan päältä,
			// ja poistamme korostukset.
			st[0].onmouseout = function(e) {
				if ($.ipad_or_ie) {
					$("#valitut_ie").hide();
				}
				if ($.vaihe == 1) {
					$("#selite").hide();
				} else if ($.vaihe == 2) {
					if (alue != $.valittu && !(alue in $.valitut)) {
						st.attr("fill", alue_vari(alue));
					}
				}
			}

			if (alue == "nsw") {
				st[0].onmouseover();
			}
		})(suomi[alue], alue);
	}


}

// Zoomaamme Suomen, kun käyttäjä klikkaa
// jotain kuntaa vaiheessa 1.
function skaalaa_suomi() {
	if ($.suomen_skaala == .17) {
		rsr.forEach(function (el) {
			el.attr("stroke-width", 1);
		});
		$("#info1").hide();
		$("#kuntavalinta").hide();
	}
	$.suomen_skaala = $.suomen_skaala + $.nopeus;
	// Pehmennetään zoomausta.
	$.nopeus = $.nopeus / 1.12;
	if ($.suomen_skaala > .6 || $.nopeus < 0.001 || $.ipad_or_ie) {
		// Lopetetaan zoomaus.
		// iPadin ja IE 8:n (tai vanhemman) tapauksessa skipataan se kokonaan.
		clearInterval($.ss);
		$("#info2").css("width","500px");
		$.valitut[$.valittu] = true;
		$.suomen_skaala = .6;
		if ($.ipad_or_ie) {
			rsr.scaleAll($.suomen_skaala);
		}
		listaa_valitut();
		$("#info2").fadeIn("fast");
	} else {
		// Skaalataan karttaa.
		rsr.scaleAll($.suomen_skaala);
	}
	// Yritetään pitää valittu kunta näkyvissä.
	var sk = $("#paper").width() / 310;
	var et_kesk_nytX = sk * $.etaisyys_keskX;
	var et_kesk_nytY = sk * $.etaisyys_keskY;
	var klik_nytX = 355 - sk * $.klikX;
	var klik_nytY = 273 - sk * $.klikY;
	$("#paper").css("left",klik_nytX + "px").css("top",klik_nytY + "px");
}

// Listataan käyttäjälle hänen valitsemansa kunnat
// vaiheessa 2.
function listaa_valitut() {
	// Lasketaam yhteen valittujen kuntien tiedot
	var tekijat = 0, tyopaikat = 0;
	$.each($.valitut, function(name) {
		if (kuntaData[name] !== undefined) {
			tekijat += kuntaData[name].tekijat;
			tyopaikat += kuntaData[name].tyopaikat;
		}
	});
	$("#tekijat_total").text(tekijat);
	$("#tyopaikat_total").text(tyopaikat);

	var omavar = tyopaikat / tekijat;
	var suhde = Math.round(omavar * 100);
	$("#suhde_total").text(suhde + "%");

	// Väritetään yhdistetty kunta tietojen mukaan
	var color_pos = (omavar - OMAVAR_MIN) / (OMAVAR_MAX - OMAVAR_MIN);
	color_pos = Math.max(0, Math.min(color_pos, 1));
	var hex = colorScale.getColor(color_pos).hex();
	$.each($.valitut, function(alue) {
		suomi[alue].attr("fill", hex);
	});
}

// Dokumentti on latautunut. Teemme kaikenlaisia alkuvalmisteluja.
$(document).ready(function(){
	var deviceAgent = navigator.userAgent.toLowerCase();
    var agentID = deviceAgent.match(/(iphone|ipod|ipad)/);
    if (agentID || $.browser.msie && parseInt($.browser.version, 10) < 9) {
         $.ipad_or_ie = true;	// Jos kyseessä on iPad tai IE <= 8,
		 						// tallennamme tiedon ja käytämme sitä
								// myöhemmin esim. skippaamaan
								// animaatioita.
     } else {
		$.ipad_or_ie = false;
	}

	// Karttaa klikatessa kirjoitetaan ylös, missä hiiri sijaitsi.
	// Tietoa tarvitaan, kun zoomatessa valittu kunta pyritään
	// pitämään keskitettynä.
	$("#paper").mousedown(function(e){
		$.klikX = e.pageX - this.offsetLeft - 140;
		$.klikY = e.pageY - this.offsetTop;
		$.etaisyys_keskX = 355 - $.klikX;
		$.etaisyys_keskY = 273 - $.klikY;
	});

	// Valitut kunnat tallennetaan tähän objektiin
	$.valitut = {};

	// Pudotusvalikon toiminnallisuus alussa.
	var names = $.map(kuntaPaths, function(path, name) { return name; });
	$("#kuntavalinta").append($.map(names.sort(), function(name, i) {
		return $("<option>").text(name)[0];
	}));

	$("#kuntavalinta").change(function(){
		var a = $(this).val();
		if (a != "0") {
			// Koska karttaa ei nyt klikattu, yritetään taikoa
			// jokin osapuilleen oikea kuvitteellinen
			// klikkauskohta zoomaamiselle.
			$.klikX = suomi[a].getBBox(true).x2 * $.suomen_skaala + 50;
			$.klikY = suomi[a].getBBox(true).y2 * $.suomen_skaala - 50;
			$.etaisyys_keskX = 355 - $.klikX;
			$.etaisyys_keskY = 273 - $.klikY;
			suomi[a].toFront();
			rsr.safari();
			$.ss = setInterval("skaalaa_suomi()",20);
			$.vaihe = 2;
			$.valittu = $(this).val();
			var x = suomi[$.valittu].getBBox(true).x2 - (suomi[$.valittu].getBBox(true).width / 5);
			var y = suomi[$.valittu].getBBox(true).y2 - (suomi[$.valittu].getBBox(true).height / 5);
		}
	});

	piirra_kartta();

});